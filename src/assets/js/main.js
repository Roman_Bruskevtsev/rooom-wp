(function($) {
    $(window).on('load', function(){
        /*Preloader*/
        var i = 1;
        function preloader(){
            var line = $('.preloader__wrapper .line');
            setTimeout(function () {    
                i++;                     
                if (i <= 100) {
                    line.css("width", i + "%");   
                    preloader();             
                }  
                if(i == 101) {
                    setTimeout(function () {  
                        $('.preloader__wrapper').addClass('hide__loader');
                    }, 500);
                }
            }, 20);
        }
        
        preloader();

        /*Get call popup*/
        $('.get__call').on('click', function(){
            $('.popup__wrapper').addClass('show__popup');
        });
        $('.close').on('click', function(){
            $('.popup__wrapper').removeClass('show__popup');
        });

        /*Main menu*/
        $('.main__menu').on('click', function(){
            $('.projects__menu').removeClass('show__bar');
            $('#projects__nav').removeClass('show__bar');

            $(this).toggleClass('show__bar');
            
            $('#main__nav').toggleClass('show__bar');
        });

        $('.menu__burger').on('click', function(){
            $('header').toggleClass('open');

            $(this).toggleClass('show__bar');
            
            $('#main__nav').toggleClass('show__bar');
        });
        /*Main Slider*/
        if($('.full__slider').length){
            $('.full__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  5000,
                vertical:       true,
                verticalSwiping:true,
                speed:          1000,
                prevArrow:      '<span class="prev__slide slide__nav"></span>',
                nextArrow:      '<span class="next__slide slide__nav"></span>'
            });

            $('.full__slider').bind('DOMMouseScroll', function(e){
                if(e.originalEvent.detail > 0) {
                    $('.full__slider').slick('slickNext');
                } else {
                    $('.full__slider').slick('slickPrev');
                }
                return false;
            });

            $('.full__slider').bind('mousewheel', function(e){
                if(e.originalEvent.wheelDelta < 0) {
                    $('.full__slider').slick('slickNext');
                } else {
                    $('.full__slider').slick('slickPrev');
                }
                return false;
            });
        }
        

        /*Padding Slider*/
        if($('.padding__slider').length){
            function sliderHeight(){
                var windowHeight = $(window).outerHeight();
                var headerHeight = $('header').outerHeight();
                var sliderHeight = windowHeight - headerHeight;

                $('.padding__slider').css('padding-top', headerHeight);
                $('.padding__slider .slide').css('height', sliderHeight);
            }
            sliderHeight();
            $(window).resize(sliderHeight);

            $('.padding__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  8000,
                vertical:       true,
                swipe:          false,
                verticalSwiping:false,
                speed:          1000,
                prevArrow:      '<span class="prev__slide slide__nav"></span>',
                nextArrow:      '<span class="next__slide slide__nav"></span>'
            });

            $('.padding__slider').bind('DOMMouseScroll', function(e){
                if(e.originalEvent.detail > 0) {
                    $('.padding__slider').slick('slickNext');
                } else {
                    $('.padding__slider').slick('slickPrev');
                }
                return false;
            });

            $('.padding__slider').bind('mousewheel', function(e){
                if(e.originalEvent.wheelDelta < 0) {
                    $('.padding__slider').slick('slickNext');
                } else {
                    $('.padding__slider').slick('slickPrev');
                }
                return false;
            });

            $('.prev__service').on('click', function(e){
                e.preventDefault();
                $('.padding__slider').slick('slickPrev');
            });
            $('.next__service').on('click', function(e){
                e.preventDefault();
                $('.padding__slider').slick('slickNext');
            });
        }

        /*Project slider*/
        if($('.project__slider').length){
            function sliderHeight(){
                var windowHeight = $(window).outerHeight();
                var headerHeight = $('header').outerHeight();
                var sliderHeight = windowHeight - headerHeight;

                $('.project__slider').css('padding-top', headerHeight);
                $('.project__slider .slide').css('height', sliderHeight);
            }
            sliderHeight();
            $(window).resize(sliderHeight);

            $('.project__slider').slick({
                infinite:       true,
                autoplay:       false,
                vertical:       true,
                swipe:          false,
                verticalSwiping:false,
                speed:          500,
                arrows:         false,
                accessibility:  false,
                draggable:      false
            });

            $('.project__navigation li').on('click', function(){
                var toSlide = $(this).data('slide');

                $(this).addClass('active');
                $('.project__navigation li').not(this).removeClass('active');

                $('.project__slider').slick('slickGoTo', toSlide);
            });
        }

        /*Room slider*/
        if($('.room__slider').length){
            $('.room__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  8000,
                speed:          1000,
                prevArrow:      '<span class="prev__slide slide__nav__middle"></span>',
                nextArrow:      '<span class="next__slide slide__nav__middle"></span>'
            });
        }

        /*Hide-show sidebar*/
        $('.hide__sidebar').on('click', function(){
            $('#project__sidebar').removeClass('show__bar');
            $('#short__description').addClass('show__bar');
        });
        $('.show__sidebar').on('click', function(){
            $('#project__sidebar').addClass('show__bar');
            $('#short__description').removeClass('show__bar');
        });

        /*Image slider*/
        if($('.image__slider').length){
            $('.image__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  8000,
                speed:          1000,
                prevArrow:      '<span class="prev__slide slide__nav"></span>',
                nextArrow:      '<span class="next__slide slide__nav"></span>'
            });
        }

        /*Google map*/
        $(function() {
            var marker = [],
                infowindow = [],
                map, image = $('.google__map').attr('data-marker');

            function addMarker(location, name, contentstr) {

                marker[name] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: image
                });
                marker[name].setMap(map);

                infowindow[name] = new google.maps.InfoWindow({
                    content: contentstr
                });

                google.maps.event.addListener(marker[name], 'click', function() {
                    infowindow[name].open(map, marker[name]);
                });
            }

            function initialize() {
                var lat = $('#google__map').attr("data-lat");
                var lng = $('#google__map').attr("data-lng");
                var mark_name = $('#google__map').attr("data-name");
                var mark_str = $('#google__map').attr("data-str");
                var setZoom = parseInt($('#google__map').attr("data-zoom"));

                var styles = [
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e9e9e9"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    }
                ];

                var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });
                var myLatlng = new google.maps.LatLng(lat, lng);

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById("google__map"), mapOptions);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');


                addMarker(myLatlng, mark_name, mark_str);

            }

            if ($('.google__map').length) {
                setTimeout(function() {
                    initialize();
                }, 500);
            }
        });

        /*Contact form 7*/
        if($('.contact__form').length){
            var contactForm = document.querySelector('.contact__form .wpcf7');

            contactForm.addEventListener('wpcf7invalid', function( event ) {
                $('.contact__form .form__message.error').addClass('show__message');
            }, false);

            contactForm.addEventListener('wpcf7mailsent', function( event ) {
                $('.contact__form .form__message.success').addClass('show__message');
            }, false); 
        }
        
        if($('.popup').length){
            var getcallForm = document.querySelector('.popup .wpcf7');
            getcallForm.addEventListener('wpcf7invalid', function( event ) {
                $('.popup .form__message.error').addClass('show__message');
            }, false);

            getcallForm.addEventListener('wpcf7mailsent', function( event ) {
                $('.popup .form__message.success').addClass('show__message');
            }, false);
        }

        $('.close__message').on('click', function(){
            $(this).closest('.form__message').removeClass('show__message');
        });
    });
})(jQuery);
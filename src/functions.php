<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 */

/*Trainers post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Theme setup*/
function rooom_setup() {
    load_theme_textdomain( 'rooom' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'project-thumbnail', 800, 684, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'rooom' ),
        'portfolio'     => __( 'Portfolio Menu', 'rooom' )
    ) );
}
add_action( 'after_setup_theme', 'rooom_setup' );

/*App2drive styles and scripts*/
function rooom_scripts() {
    $version = '1.0.2';

    wp_enqueue_style( 'rooom-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'rooom-style', get_stylesheet_uri() );


    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5ae5db7fde20620011e0349f&product=custom-share-buttons', array('jquery'), $version, true );
    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'rooom_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Main Page Settings',
    //     'menu_title'    => 'Main Page',
    //     'parent_slug'   => $general['menu_slug']
    // ));
}


/*SVG support*/
function rooom_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'rooom_svg_types');

/*App2drive ajax*/
function rooom_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}
add_action('wp_footer','rooom_ajaxurl');

function rooom_no_limit_posts( $query ) {
    $query->set( 'posts_per_page', '-1' );
}
add_action( 'pre_get_posts', 'rooom_no_limit_posts' );
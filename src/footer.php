<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <div class="popup__wrapper">
        <div class="popup text-center">
            <span class="close"></span>
            <?php if( get_field('callback_title', 'option') ){ ?><h3><?php the_field('callback_title', 'option'); ?></h3><?php } ?>
            <?php if( get_field('form_shortcode', 'option') ) { ?>
            <div class="call_back">
                <?php echo do_shortcode( get_field('form_shortcode', 'option') ); ?>
            </div>
            <?php } ?>
            <div class="form__message success">
                <span class="close__message"></span>
                <div class="form__content">
                    <h4><?php echo THANKS; ?></h4>
                    <p><?php echo GOTMESSAGE; ?></p>
                </div>
            </div>
            <div class="form__message error">
                <span class="close__message"></span>
                <div class="form__content">
                    <h4><?php echo OOPS; ?></h4>
                    <p><?php echo TRYAGAIN; ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>

</body>
</html>
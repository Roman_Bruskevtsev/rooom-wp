<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'main_page_slider' ): 
            get_template_part( 'inc/acf-content/main-page-slider' );
        elseif( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'inc/acf-content/about-us-section' );
        elseif( get_row_layout() == 'contacts_section' ): 
            get_template_part( 'inc/acf-content/contacts-section' );
        elseif( get_row_layout() == 'services_section' ): 
            get_template_part( 'inc/acf-content/services-section' );
        endif;
    endwhile;
else :
    echo '
        <section class="padding__section">
            <div class="page__content">
                <div class="no__content">
                    <h1>'.__('Nothing to show', 'rooom').'</h1>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();
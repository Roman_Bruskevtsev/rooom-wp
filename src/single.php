<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<section class="padding__section">
    <div id="short__description" class="small__sidebar show__bar">
        <h2><?php the_title(); ?></h2>
        <ul class="social__links">
            <li>
                <span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                </span>
            </li>
            <li>
                <span class="pinterest st-custom-button" data-network="pinterest" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                </span>
            </li>
        </ul>
        <div class="show__sidebar"></div>
    </div>
    <div id="project__sidebar" class="sidebar__nav large">
        <div class="project__wrapper">
            <div class="project__info">
                <h1><?php the_title(); ?></h1>
                <span class="location"><?php the_field('location'); ?></span>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="bottom__row">
            <div class="hide__sidebar"><span><?php echo HIDE; ?></span></div>
            <div class="sharing__block">
                <span><?php echo SHARE; ?></span>
                <ul class="social__links">
                    <li>
                        <span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                    <li>
                        <span class="pinterest st-custom-button" data-network="pinterest" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php if( have_rows('project_part') ): 
    $i = 0;
    ?>
    <ul class="project__navigation">
        <?php while ( have_rows('project_part') ) : the_row(); 
        $active = ( $i == 0 ) ? ' class="active"' : '';
        ?>
        <li<?php echo $active; ?> data-slide="<?php echo $i; ?>">
            <span></span>
            <h6><?php the_sub_field('title'); ?></h6>
        </li>
        <?php $i++; endwhile; ?>
    </ul>
    <?php endif;
    if( have_rows('project_part') ): ?>
    <div class="project__slider">
        <?php while ( have_rows('project_part') ) : the_row(); ?>
        <div class="slide project__slide">
            <?php
            $gallery = get_sub_field('images');
            if( $gallery ) { ?>
            <div class="room__slider">
                <?php foreach( $gallery as $image ): ?>
                    <div class="slide" style="background-image: url(<?php echo $image['url']; ?>);"></div>
                <?php endforeach; ?>
            </div>
            <?php } ?>
        </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
</section>

<?php get_footer();
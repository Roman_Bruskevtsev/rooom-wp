<?php
add_action('init', 'rooom_trainers_post', 0);

function rooom_trainers_post() {
  //Register taxonomy
  $taxonomy_labels = array(
    'name'                        => __('Categories', 'escort'),
    'singular_name'               => __('Categories', 'escort'),
    'menu_name'                   => __('Categories', 'escort')
  );

  $taxonomy_rewrite = array(
    'slug'                  => 'projects-categories',
    'with_front'            => true,
    'hierarchical'          => true,
  );

  $taxonomy_args = array(
    'labels'              => $taxonomy_labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'show_in_nav_menus'   => true,
    'show_tagcloud'       => true,
    'rewrite'             => $taxonomy_rewrite,
  );
  register_taxonomy( 'projects-categories', 'project', $taxonomy_args );

  //Register new post type
  $projects_labels = array(
  'name'                => __('Projects', 'rooom'),
  'add_new'             => __('Add Project', 'rooom')
  );

  $projects_args = array(
  'label'               => __('Projects', 'rooom'),
  'description'         => __('Project information page', 'rooom'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'thumbnail', 'editor', 'excerpt'),
  'taxonomies'          => array( 'projects-categories' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-images-alt2'
  );
  register_post_type( 'project', $projects_args );
}

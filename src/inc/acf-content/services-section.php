<?php
$services = [];
$i = 0;

if( have_rows('service') ): 
    while ( have_rows('service') ) : the_row(); 
        $services[] = get_sub_field('title');
    endwhile;
endif; ?>

<section class="padding__section">
    <?php if( have_rows('service') ): ?>
    <div class="padding__slider left__nav">
        <?php while ( have_rows('service') ) : the_row(); 
        $next_slide = $i + 1;
        if( $next_slide == count($services) ) { 
            $next_slide = $services[0];
        } else {
            $next_slide = $services[$next_slide];
        }

        $prev_slide = $i - 1;
        if( $i == 0 ) {
            $prev_slide = end($services);
        } else {
            $prev_slide = $services[$prev_slide];
        } 
        ?>

        <div class="slide">
            <a href="#" class="service__nav prev__service"><?php echo $prev_slide; ?></a>
            <a href="#" class="service__nav next__service"><?php echo $next_slide; ?></a>
            <div class="image__block float-right">
                <?php
                $gallery = get_sub_field('images');
                if( $gallery ) { ?>
                <div class="image__slider">
                    <?php foreach( $gallery as $image ): ?>
                        <div class="slide" style="background-image: url(<?php echo $image['sizes']['large']; ?>);"></div>
                    <?php endforeach; ?>
                </div>
                <?php } ?>
            </div>
            <div class="text__block float-left">
                <div class="content__wrapper">
                    <div class="text__content">
                        <?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
                        <?php the_sub_field('text'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $i++;
        endwhile; ?>
    </div>
<?php endif; ?>
</section>
<section class="padding__section">
    <div class="page__content">
        <div class="half__block">
            <?php if( get_sub_field('map_zoom') && get_sub_field('marker') && get_sub_field('latitude') && get_sub_field('longitude') ) { ?>
            <div id="google__map" class="google__map" data-lat="<?php the_sub_field('latitude'); ?>" data-lng="<?php the_sub_field('longitude'); ?>" data-zoom="<?php the_sub_field('map_zoom'); ?>" data-marker="<?php the_sub_field('marker'); ?>" data-name="<h3>Rooom</h3>" data-str="<h5><?php bloginfo( 'name' ); ?></h5><?php the_sub_field('text'); ?>"></div>
            <?php } ?>
            <?php if( get_sub_field('form_shortcode') ) { ?>
            <div class="contact__form">
                <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
                <div class="form__message success">
                    <span class="close__message"></span>
                    <div class="form__content">
                        <h4><?php echo THANKS; ?></h4>
                        <p><?php echo GOTMESSAGE; ?></p>
                    </div>
                </div>
                <div class="form__message error">
                    <span class="close__message"></span>
                    <div class="form__content">
                        <h4><?php echo OOPS; ?></h4>
                        <p><?php echo TRYAGAIN; ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="half__block">
            <div class="content__wrapper">
                <div class="content text-left">
                    <?php if( get_sub_field('title') ) { ?><h1><?php the_sub_field('title'); ?></h1><?php } ?>
                    <?php the_sub_field('text'); ?>
                    <?php if( have_rows('phones') ): ?>
                    <div class="phone__list">
                        <?php while ( have_rows('phones') ) : the_row(); ?>
                        <a href="tel:<?php the_sub_field('phone_number'); ?>"><?php the_sub_field('phone_number'); ?></a>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                    <?php if( get_sub_field('title') ) { ?>
                    <div class="email__list">
                        <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
                    </div>
                    <?php } ?>
                    <div class="social__block">
                        <?php if( get_sub_field('folow_us_text') ) { ?><span><?php the_sub_field('folow_us_text'); ?></span><?php } ?>
                        <ul class="social__links">
                            <?php if( get_field('instagram', 'option') ) { ?>
                            <li>
                                <a href="<?php the_field('instagram', 'option'); ?>" class="instagram"></a>
                            </li>
                            <?php }
                            if( get_field('facebook', 'option') ) { ?>
                            <li>
                                <a href="<?php the_field('facebook', 'option'); ?>" class="facebook"></a>
                            </li>
                            <?php }
                            if( get_field('pinterest', 'option') ) { ?>
                            <li>
                                <a href="<?php the_field('pinterest', 'option'); ?>" class="pinterest"></a>
                            </li>
                            <?php }
                            if( get_field('behance', 'option') ) { ?>
                            <li>
                                <a href="<?php the_field('behance', 'option'); ?>" class="behance"></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
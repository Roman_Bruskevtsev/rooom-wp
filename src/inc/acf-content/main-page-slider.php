<?php if( have_rows('slide') ): ?>
<div class="full__slider">
    <?php while ( have_rows('slide') ) : the_row(); 
    $background = (get_sub_field('background_image')) ? 'style="background-image: url('.get_sub_field('background_image').');"' : '';
    ?>
    <div class="slide"<?php echo $background; ?>>
        <?php if( get_sub_field('link') ) { ?><a href="<?php the_sub_field('link'); ?>"><?php } ?>
            <?php if( get_sub_field('text') ) { ?>
            <div class="content">
                <h2><?php the_sub_field('text'); ?></h2>
            </div>
            <?php } ?>
        <?php if( get_sub_field('link') ) { ?></a><?php } ?>
    </div>
    <?php endwhile; ?>
</div>
<?php endif; ?>
<section class="padding__section">
    <div class="page__content">
        <?php $background = (get_sub_field('image')) ? 'style="background-image: url('.get_sub_field('image').');"' : ''; ?>
        <div class="half__block"<?php echo $background; ?>></div>
        <div class="half__block">
            <div class="content__wrapper">
                <div class="content text-center">
                    <?php if(get_sub_field('title')) { ?><h1 class="text-center"><?php the_sub_field('title'); ?></h1><?php } ?>
                    <?php the_sub_field('text'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
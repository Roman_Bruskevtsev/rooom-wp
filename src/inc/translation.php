<?php
$current_lang = ICL_LANGUAGE_CODE;

if($current_lang == 'uk'){
    define("SHARE",               "Поширити:");
    define("HIDE",                "згорнути");
    define("THANKS",              "Дякуємо!");
    define("GOTMESSAGE",          "Ми отримали ваше повідомлення і зателефонуємо вам якомога швидше!");
    define("OOPS",                "Ой лишенько!");
    define("TRYAGAIN",            "Заповніть обов'язкові поля та повторіть знову!");
} else {
    define("SHARE",               "Share:");
    define("HIDE",                "hide");
    define("THANKS",              "Thanks!");
    define("GOTMESSAGE",          "We have received your message and will call you as soon as possible!");
    define("OOPS",                "Oops!");
    define("TRYAGAIN",            "Fill in the required fields and try again!");
}
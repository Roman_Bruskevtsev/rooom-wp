<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$i = 1;
?>

<section class="padding__section">
    <?php if ( have_posts() ) : ?>
    <div class="padding__slider">
        <?php while ( have_posts() ) : the_post(); 
        $thumb = (get_the_post_thumbnail( get_the_ID() )) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'project-thumbnail' ).')"' : '';
        if( $i == 1 or ($i % 8) == 1 ) { ?><div class="slide"><?php } ?>
            <a href="<?php the_permalink(); ?>" class="project__block <?php echo 'project__id__'.get_the_ID(); ?>"<?php echo $thumb; ?>>
                <div class="project__info">
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_field('location'); ?></p>
                </div>
            </a>
        <?php if( $i % 8 == 0 ) { ?></div><?php }
        $i++;
        endwhile ?>
    </div>
    <?php endif; ?>
</section>

<?php get_footer();
<?php
/**
 *
 * @package WordPress
 * @subpackage Rooom
 * @since 1.0
 * @version 1.0
 */
$header__class = (!is_front_page()) ? ' class="dark"' : ''; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>

</head>
<body <?php body_class();?>>
    <div class="preloader__wrapper">
        <?php if( get_field('preloader_image', 'option') ){ ?>
        <div class="logo">
            <img src="<?php the_field('preloader_image', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
        </div>
        <?php } ?>
        <div class="line"></div>
    </div>
    <header id="header"<?php echo $header__class; ?>>
        <div class="nav__row">
            <div class="nav__block float-left">
                <div class="menu__burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <?php if( get_field('logo', 'option') ){ ?>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo__block">
                <img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
            </a>
            <?php } ?>
            <div class="lang__block float-right">
                <div class="get__call">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/get_call.svg" alt="Get call">
                </div>
                <?php 
                $wpml_lang = icl_get_languages();
                $cur_lang_li = '';
                $all_lang_li = '';
                foreach ($wpml_lang as $lang ) {
                    if($lang['active']){
                        $cur_lang_li = '
                                        <a href="'.$lang['url'].'" class="active wpml_'.$lang['code'].'">'.$lang['code'].'
                                        </a>
                                        <span class="sep"></span>';
                    } else {
                        $all_lang_li .= '
                                        <a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">'.$lang['code'].'
                                        </a>
                                        ';
                    }
                } ?>

                <div class="lang__switcher">
                    <?php echo $cur_lang_li; ?>
                    <?php echo $all_lang_li; ?>
                </div>
            </div>
        </div>
    </header>
    <div id="main__nav" class="full__nav">
        <div class="nav__row">
            <div class="nav__col">
                <div class="nav__block bottom">
                    <?php wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'main__nav'
                    ) ); ?>
                </div>
                <div class="line"></div>
            </div>
            <div class="nav__col">
                <div class="nav__block top">
                    <?php wp_nav_menu( array(
                        'theme_location'        => 'portfolio',
                        'container'             => 'nav',
                        'container_class'       => 'portfolio__nav'
                    ) ); ?>
                </div>
            </div>
        </div>
        <ul class="social__links">
            <?php if( get_field('instagram', 'option') ) { ?>
            <li>
                <a href="<?php the_field('instagram', 'option'); ?>" class="instagram"></a>
            </li>
            <?php }
            if( get_field('facebook', 'option') ) { ?>
            <li>
                <a href="<?php the_field('facebook', 'option'); ?>" class="facebook"></a>
            </li>
            <?php }
            if( get_field('pinterest', 'option') ) { ?>
            <li>
                <a href="<?php the_field('pinterest', 'option'); ?>" class="pinterest"></a>
            </li>
            <?php }
            if( get_field('behance', 'option') ) { ?>
            <li>
                <a href="<?php the_field('behance', 'option'); ?>" class="behance"></a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <main>
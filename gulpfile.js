'use strict';
var themeName   = 'rooom',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    babel       = require('gulp-babel'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    jsmin       = require('gulp-minify'),
    imagemin    = require('gulp-imagemin'),
    rename      = require('gulp-rename'),
    rigger      = require('gulp-rigger'),
    browserSync = require('browser-sync'),
    concat      = require('gulp-concat'),
    buildFolder = 'build/wp-content/themes/' + themeName,
    buildPlugin = 'build/plugins',
    reload      = browserSync.reload;

gulp.task('styles', function () {
    gulp.src('src/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
    gulp.src('src/style.css')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('scripts', function () {
    gulp.src('src/assets/js/*.js')
        .pipe(jsmin({
            ext:{
                src:'-debug.js',
                min:'.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest(buildFolder + '/assets/js/'));
        
});

gulp.task('html', function () {
    gulp.src('src/**/*.html')
        .pipe(rigger())
        .pipe(gulp.dest(buildFolder))
        .pipe(reload({stream: true}));
});

gulp.task('php', function () {
    gulp.src('src/**/*.php')
        .pipe(gulp.dest(buildFolder))
        .pipe(reload({stream: true}));
});

gulp.task('images', function() {
    gulp.src('src/assets/images/**/*')
        .pipe(gulp.dest(buildFolder + '/assets/images'));
    gulp.src('src/screenshot.png')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('default', [
    'styles',
    'scripts',
    'html',
    'php',
    'images'
]);

gulp.task('watch', function () {
    gulp.watch('src/assets/sass/**/*.scss', ['styles']);

    gulp.watch('src/assets/js/**/*.js', ['scripts']);

    gulp.watch('src/**/*.html', ['html']);

    gulp.watch('src/**/*.php', ['php']);
});